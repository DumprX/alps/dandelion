#
# Copyright (C) 2022 The Android Open Source Project
# Copyright (C) 2022 SebaUbuntu's TWRP device tree generator
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/omni_garden.mk

COMMON_LUNCH_CHOICES := \
    omni_garden-user \
    omni_garden-userdebug \
    omni_garden-eng
